(function() {
    window.onscroll = function() {stickyHeader()};
    let body = document.getElementById("neve_body");
    let sticky = body.offsetTop;

    function stickyHeader() {
        if (window.pageYOffset > sticky) {
            body.classList.add("sticky-header");
        } else {
            body.classList.remove("sticky-header");
        }
    }
}());