# TobiePolsko

UWAGA! Wymagany jest git: https://git-scm.com/downloads

### Pobranie projektu
* Otwieramy konsolę gita
* Wchodzimy do katalogu projektu (wordpress-docker), a następnie:
```
cd wordpress/wp-content/themes
```
* klonujemy projekt:
```
git clone git@gitlab.com:janzaba/tobiepolsko.git neve-child
```
* stworzy się katalog `neve-child` i od teraz child-theme można już włączyć w wordpressie

### Praca na plikach
* przed dokonaniem zmian należy przejść na nowy branch, jego nazwa może być od tego co chcemy zrobić lub od nazwiska osoby wprowadzającej zmianę:
```
git checkout -b nazwa-brancha
```
* można teraz wprowadzać zmiany w plikach
* po zakończeniu etapu prac robimy commit:
```
git add -A
git commit -m "Informacja co zostało zrobione"
```
* wysyłamy zmiany do repozytorium
```
git push origin nazwa-brancha
```
* w konsoli pojawią się komunikaty o statusie wysyłania zmian, oraz link do stworzenia Merge Request,
wchodzimy w ten link i zatwierdzamy, dzięki temu właściciel repozytorium otrzyma możliwość komentowania
zaproponowanego kodu i jego zatwierdzenia jeśli będzie ok

### Aktualizacja zmian lokalnie
* Jeśli zostaną zatwierodzne czyjeś zmiany, wszyscy będą mogli je pobrać
* aby to zrobić wywołaj komendy:
```
# wracamy na główny branch
git checkout master 

# pobieramy zmiany
git pull origin master

# można teraz wrócić na swój branch
git checkout nazwa-brancha

# należy do swojego brancha dodać zmiany z brancha głównego
git rebase master
```
