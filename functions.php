<?php

add_action( 'wp_enqueue_scripts', 'neve_child_enqueue', 102 );
function neve_child_enqueue () {
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Pirata+One&family=Oswald&display=swap', false );
    wp_enqueue_style( 'neve-main-style', get_stylesheet_directory_uri() . '/style.css', false, '1.0', 'all' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array( 'neve-main-style' ), '1.1.4');
    wp_register_style( 'calendar', get_stylesheet_directory_uri() . '/assets/css/calendar.css', array( 'child-style' ), wp_get_theme()->get('Version'));
    wp_register_style( 'slick', get_stylesheet_directory_uri() . '/assets/css/slick.css', array( 'child-style' ), wp_get_theme()->get('Version'));
 
	wp_register_script( 'calendar-utils', get_stylesheet_directory_uri() . '/assets/js/calendar/util.js', array ('jquery'), 1.0, true);
    wp_register_script( 'calendar-main', get_stylesheet_directory_uri() . '/assets/js/calendar/main.js', array ('jquery'), 1.0, true);
	wp_register_script( 'slick', get_stylesheet_directory_uri() . '/assets/js/slick.min.js', array ('jquery'), 1.0, true);
	wp_register_script( 'partners-slick', get_stylesheet_directory_uri() . '/assets/js/main.js', array ('jquery'), 1.0, true);
	wp_enqueue_script( 'scripts-child', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array ('jquery'), 1.0, true);
}

add_action('after_setup_theme', 'reset_parent_setup', 999);

function reset_parent_setup()
{
    add_image_size( 'neve-blog', 1024, 768, true );
}

/** Custom Post Types */
include_once "setup/cpt/cpt.php";

/** Shortcodes */
include_once "setup/shortcodes/shortcodes.php";

/** ACF custom fields setup */
include_once "setup/acf/acf.php";