<?php

function partners_post_type() {

    $labels = array(
        'name'                => _x( 'Partnerzy', 'Post Type General Name', 'neve-child' ),
        'singular_name'       => _x( 'Partner', 'Post Type Singular Name', 'neve-child' ),
        'menu_name'           => __( 'Partnerzy', 'neve-child' ),
        'parent_item_colon'   => __( 'Nadrz�dny partner', 'neve-child' ),
        'all_items'           => __( 'Wszyscy pertnerzy', 'neve-child' ),
        'view_item'           => __( 'Zobacz partnera', 'neve-child' ),
        'add_new_item'        => __( 'Dodaj nowego partnera', 'neve-child' ),
        'add_new'             => __( 'Dodaj nowego', 'neve-child' ),
        'edit_item'           => __( 'Edytuj partnera', 'neve-child' ),
        'update_item'         => __( 'Aktualizuj partnera', 'neve-child' ),
        'search_items'        => __( 'Wyszukaj partnera', 'neve-child' ),
        'not_found'           => __( 'Nie znaleziono', 'neve-child' ),
        'not_found_in_trash'  => __( 'Nie znaleziono w koszu', 'neve-child' ),
    );

    $args = array(
        'label'               => __( 'partnerzy', 'neve-child' ),
        'description'         => __( 'Lista partner�w', 'neve-child' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'partners', $args );
}

add_action( 'init', 'partners_post_type', 0 );