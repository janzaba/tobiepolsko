<?php

function gra_post_type() {

    $labels = array(
        'name'                => _x( 'Gra', 'Post Type General Name', 'neve-child' ),
        'singular_name'       => _x( 'Gra', 'Post Type Singular Name', 'neve-child' ),
        'menu_name'           => __( 'Gra', 'neve-child' ),
        'parent_item_colon'   => __( 'Gra', 'neve-child' ),
        'all_items'           => __( 'Wszystkie wpisy gra', 'neve-child' ),
        'view_item'           => __( 'Zobacz wpis', 'neve-child' ),
        'add_new_item'        => __( 'Dodaj nowy wpis', 'neve-child' ),
        'add_new'             => __( 'Dodaj nowy', 'neve-child' ),
        'edit_item'           => __( 'Edytuj', 'neve-child' ),
        'update_item'         => __( 'Aktualizuj', 'neve-child' ),
        'search_items'        => __( 'Wyszukaj', 'neve-child' ),
        'not_found'           => __( 'Nie znaleziono', 'neve-child' ),
        'not_found_in_trash'  => __( 'Nie znaleziono w koszu', 'neve-child' ),
    );

    $args = array(
        'label'               => __( 'gra', 'neve-child' ),
        'description'         => __( 'Lista wpisów', 'neve-child' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'gra', $args );
}

add_action( 'init', 'gra_post_type', 0 );