<?php

function events_taxonomies() {
    register_taxonomy(
        'events_category',
        array('events'),
        array(
            'hierarchical' => true,
            'labels' => array(
                'name' => 'Kategorie wydarzeń',
                'add_new_item' => 'Dodaj nową kategorię',
                'new_item_name' => "Nowa kategoria"
            ),
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'events_category' ),
            'show_in_rest' => true,
        )
    );

    register_taxonomy(
        'events_days',
        array('events'),
        array(
            'hierarchical' => true,
            'labels' => array(
                'name' => 'Daty wydarzeń',
                'add_new_item' => 'Dodaj nową datę',
                'new_item_name' => "Nowa data"
            ),
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'events_dates' ),
            'show_in_rest' => true,
        )
    );
}
add_action( 'init', 'events_taxonomies', 0 );

function events_post_type() {

    $labels = array(
        'name'                => _x( 'Wydarzenia', 'Post Type General Name', 'neve-child' ),
        'singular_name'       => _x( 'Wydarzenie', 'Post Type Singular Name', 'neve-child' ),
        'menu_name'           => __( 'Wydarzenia', 'neve-child' ),
        'parent_item_colon'   => __( 'Nadrzędne wydarzenie', 'neve-child' ),
        'all_items'           => __( 'Wszystkie wydarzenia', 'neve-child' ),
        'view_item'           => __( 'Zobacz wydarzenie', 'neve-child' ),
        'add_new_item'        => __( 'Dodaj nowe wydarzenie', 'neve-child' ),
        'add_new'             => __( 'Dodaj nowe', 'neve-child' ),
        'edit_item'           => __( 'Edytuj wydarzenie', 'neve-child' ),
        'update_item'         => __( 'Aktualizyj wydarzenie', 'neve-child' ),
        'search_items'        => __( 'Wyszukaj wydarzenie', 'neve-child' ),
        'not_found'           => __( 'Nie znaleziono', 'neve-child' ),
        'not_found_in_trash'  => __( 'Nie znaleziono w koszu', 'neve-child' ),
    );

    $args = array(
        'label'               => __( 'wydarzenia', 'neve-child' ),
        'description'         => __( 'Elementy programu', 'neve-child' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'custom-fields', ),
        'taxonomies'          => array( 'events_category' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'events', $args );
}

add_action( 'init', 'events_post_type', 0 );