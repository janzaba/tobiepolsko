<?php


function partners_shortcode_func( $atts ) {
	wp_enqueue_style('slick');
	wp_enqueue_script('slick');
	wp_enqueue_script('partners-slick');
	
    $a = shortcode_atts( array(
        'foo' => 'something',
        'bar' => 'something else',
    ), $atts );

    ob_start();
  
	$args = [
		'post_type' => 'partners',
		'posts_per_page' => -1
	];
	// the query
$the_query = new WP_Query( $args ); ?>
 
<?php if ( $the_query->have_posts() ) : ?>
 
	<div class="partners-slide">
    <!-- pagination here -->
 
    <!-- the loop -->
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <a href="<?php the_field('link'); ?>">
		<?php the_post_thumbnail('thumbnail'); ?>
		</a>
	<?php endwhile; ?>
    <!-- end of the loop -->
	</div>
    <!-- pagination here -->
 
    <?php wp_reset_postdata(); ?>
 
<?php endif; ?>

    <?php
    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}
add_shortcode( 'partners', 'partners_shortcode_func' );