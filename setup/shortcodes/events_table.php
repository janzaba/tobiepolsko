<?php


function events_table_func( $atts ) {
    wp_enqueue_style( 'calendar');
    wp_enqueue_script( 'calendar-utils');
    wp_enqueue_script( 'calendar-main');

    $a = shortcode_atts( array(
        'foo' => 'something',
        'bar' => 'something else',
    ), $atts );

    ob_start();

    $dates = get_terms('events_days', array(
        'hide_empty' => true,
    ));

    ?>
    <script>document.getElementsByTagName("html")[0].className += " js";</script>

    <?php

    ?>
    <div class="cd-schedule cd-schedule--loading margin-top-lg margin-bottom-lg js-cd-schedule">
        <div class="cd-schedule__timeline">
            <ul>
                <li><span>06:00</span></li>
                <li><span>06:30</span></li>
                <li><span>07:00</span></li>
                <li><span>07:30</span></li>
                <li><span>08:00</span></li>
                <li><span>08:30</span></li>
                <li><span>09:00</span></li>
                <li><span>09:30</span></li>
                <li><span>10:00</span></li>
                <li><span>10:30</span></li>
                <li><span>11:00</span></li>
                <li><span>11:30</span></li>
                <li><span>12:00</span></li>
                <li><span>12:30</span></li>
                <li><span>13:00</span></li>
                <li><span>13:30</span></li>
                <li><span>14:00</span></li>
                <li><span>14:30</span></li>
                <li><span>15:00</span></li>
                <li><span>15:30</span></li>
                <li><span>16:00</span></li>
                <li><span>16:30</span></li>
                <li><span>17:00</span></li>
                <li><span>17:30</span></li>
                <li><span>18:00</span></li>
                <li><span>18:30</span></li>
                <li><span>19:00</span></li>
                <li><span>19:30</span></li>
                <li><span>20:00</span></li>
                <li><span>20:30</span></li>
                <li><span>21:00</span></li>
                <li><span>21:30</span></li>
                <li><span>22:00</span></li>
                <li><span>22:30</span></li>
                <li><span>23:00</span></li>
                <li><span>23:30</span></li>
                <li><span>00:00</span></li>
            </ul>
        </div> <!-- .cd-schedule__timeline -->

        <div class="cd-schedule__events">
            <ul>

                <?php
                    $i = 1;
                    foreach ($dates as $date):
                        ?>
                <li class="cd-schedule__group">
                    <div class="cd-schedule__top-info"><span><?= $date->name; ?></span></div>
                    <ul>
                        <?php
                        $events = new WP_Query([
                            'post_type'			=> 'events',
                            'posts_per_page'	=> -1,
                            'meta_key'			=> 'rozpoczecie',
                            'orderby'			=> 'meta_value_num',
                            'order'				=> 'ASC',

                            'tax_query' => [
                                [
                                    'taxonomy' => 'events_days',
                                    'field' => 'slug',
                                    'terms' => [$date->slug],
                                ],
                            ],
                        ]);
                        while ($events->have_posts()):
                            $events->the_post();

                            $types = get_the_terms(get_the_ID(), 'events_category');
                            $type = end($types);
                            $width = get_field('width') ?: 100;
                            $class = $width < 50 ? 'hide-time' : '';
                        ?>
                            <li class="cd-schedule__event <?= $class ?>" style="width: <?= $width; ?>%;<?php
                                     if (get_field('column')):
                                         $left = (get_field('column')-1) * get_field('width');
                                         ?>left: <?= $left; ?>%;<?php
                                     endif;?> ">
                                <a
                                        data-start="<?php the_field('rozpoczecie') ?>"
                                        data-end="<?php the_field('zakonczenie') ?>"
                                        data-content="<?php the_permalink(); ?>"
                                        data-event="event-<?= $i; ?>" href="#0"
                                        style="<?php
                                            if (get_field('bg_color', $type)):
                                                ?>background-color: <?= get_field('bg_color', $type); ?>;<?php
                                            endif; ?>"
                                >
                                    <em class="cd-schedule__name"><?php the_title(); ?></em>
                                </a>
                            </li>
                        <?php
                        endwhile;
                        wp_reset_postdata();
                    ?>
                    </ul>
                </li>
                    <?php
                    endforeach;
                ?>

            </ul>
        </div>

        <div class="cd-schedule-modal">
            <header class="cd-schedule-modal__header">
                <div class="cd-schedule-modal__content">
                    <span class="cd-schedule-modal__date"></span>
                    <h3 class="cd-schedule-modal__name"></h3>
                </div>

                <div class="cd-schedule-modal__header-bg"></div>
            </header>

            <div class="cd-schedule-modal__body">
                <div class="cd-schedule-modal__event-info"></div>
                <div class="cd-schedule-modal__body-bg"></div>
            </div>

            <a href="#0" class="cd-schedule-modal__close text-replace">Close</a>
        </div>

        <div class="cd-schedule__cover-layer"></div>
    </div> <!-- .cd-schedule -->
    <?php

    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}
add_shortcode( 'events_table', 'events_table_func' );