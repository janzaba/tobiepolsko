<?php


if (function_exists('acf_add_local_field_group')):

    acf_add_local_field_group(array(
        'key' => 'group_5fff1fa1a65bb',
        'title' => 'Wydarzenia',
        'fields' => array(
            array(
                'key' => 'field_5fff48adadabe',
                'label' => 'Rozpoczęcie',
                'name' => 'rozpoczecie',
                'type' => 'time_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '25',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'H:i',
                'return_format' => 'H:i',
            ),
            array(
                'key' => 'field_5fff49286d4e2',
                'label' => 'Zakończenie',
                'name' => 'zakonczenie',
                'type' => 'time_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '25',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'H:i',
                'return_format' => 'H:i',
            ),
            array(
                'key' => 'field_60105d105b1d6',
                'label' => 'Liczba kolumn',
                'name' => 'width',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '25',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    100 => '1',
                    50 => '2',
                    33 => '3',
                    25 => '4',
                    20 => '5',
                    '16.5' => '6',
                ),
                'default_value' => 100,
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'return_format' => 'value',
                'ajax' => 0,
                'placeholder' => '',
            ),
            array(
                'key' => 'field_60105d685b1d7',
                'label' => 'Nr kolumny',
                'name' => 'column',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '25',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                    5 => '5',
                    6 => '6',
                ),
                'default_value' => 1,
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'return_format' => 'value',
                'ajax' => 0,
                'placeholder' => '',
            ),

            array(
                'key' => 'field_5fff47b1602e8',
                'label' => 'Empty field',
                'name' => '',
                'type' => 'message',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'new_lines' => 'wpautop',
                'esc_html' => 0,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'events',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

    acf_add_local_field_group(array(
        'key' => 'group_6010669c32f3a',
        'title' => 'Kategoria wydarzenia',
        'fields' => array(
            array(
                'key' => 'field_601066ae68c78',
                'label' => 'Kolor tła',
                'name' => 'bg_color',
                'type' => 'color_picker',
                'instructions' => 'przykłady:
żółty: #F6B066
szary: #588093
granatowy: #433352
zielony: #A2B9B2',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'taxonomy',
                    'operator' => '==',
                    'value' => 'events_category',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;